const test = (param1,param2,callback) => {
    console.log(param1, param2)
    callback()
}

const callBack = ()=> {
    console.log("ini callback")
}

test("1","2", callBack)

const a = () =>{
    console.log("a")
}

const b = () => {
    setTimeout(() => {
        console.log("b")
    }, 1000);
}

const c = () => {
    console.log("c")
}


a()
b()
c()