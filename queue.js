// Terdapat array antrian yang berisi urutan antrian anda.
// Buatlah sebuah fungsi yang menerima parameter berupa array posisi anda dan orang lain.
// Fungsi ini akan mereturn posisi anda. Jika anda berada di index 0, berarti anda ada di depan dan begitu pula sebaliknya.
// Input => [strangers,strangers,strangers,you], [you, strangers,strangers,strangers], [strangers,strangers,strangers,you,strangers,strangers,strangers,strangers]
// Output => "anda berada di antrian paling belakang", "anda berada di antrian paling depan", "anda berada di antrian nomor 4"

const queue = () => {
  //tuliskan kode disini

};

// kode dibawah jangan diedit
function Test(fun, result) {
  console.log(fun === result, fun);
}

Test(
  queue([
    strangers,
    strangers,
    strangers,
    you,
    strangers,
    strangers,
    strangers,
    strangers,
  ]),
  "anda berada di antrian nomor 4"
);
Test(
  queue([you, strangers, strangers, strangers, strangers]),
  "anda berada di antrian paling depan"
);
Test(
  queue([strangers, strangers, , strangers, strangers, strangers, you]),
  "anda berada di antrian paling belakang"
);

Test(
  queue([
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    you,
    strangers,
    strangers,
  ]),
  "anda berada di antrian nomor 14"
);
