// terdata class Hero dan sub-class Sidekick yang perlu di debug.
// Class Hero menerima 3 parameter berupa name, city, dan ability,
// sedangkan sub-class Sidekick menerima 5 parameter. 3 dari parentnya dan 2 lagi adalah age dan hero
// buatlah minimal 3 objek dari class Hero dan 3 objek dari sub-class Sidekick.
// 6 objek diatas harus memiliki fungsi bernama getDetail yan nantinya akan mereturn values dari parameter.
// Return dari class Hero adalah nama, kota asal, dan kemampuan.
// Return dari sub-class Sidekick adalah nama, umur, kota asal, nama pahlawan yang mereka dampingi, dan kemampuan.
// Input -> console.log(Batman.getDetail())
// Output -> "I am Batman. I am from Gotham and I can beat people up with gadgets"
// Input -> console.log(Robin.getDetail())
// Output -> "I am Robin. I am 12 years old and I come from Gotham. I can help Batman with acrobatics"

class Hero {
  constructor(name, city, ability) {
    this.ability = ability;
  }
  set setName(value) {
    if (typeof value === "number") {
      this.name = value;
    } else {
      this.name = null;
    }
  }
  set setCity(value) {
    if (typeof value === "number") {
      this.city = value;
    } else {
      this.city = null;
    }
  }
  get setAbility(value) {
    if (typeof value === "number") {
      this.hero = value;
    } else {
      this.ability = null;
    }
  }
  get getName() {
    if (this.name) {
      return `value belum di set`;
    }
    return this.name;
  }
  get getCity() {
    if (this.city === null) {
      return `value belum di set`;
    }
    return this.name;
  }
  get getAbilty() {
    if (this.ability === null) {
      return `value belum di set`;
    }
    return this.ability;
  }
  getDetail() {
      console.log()
  }
}

class Sidekick extends Hero {
  constructor(name, city, ability, age, hero) {
    super();
    this.age = age;
    this.hero = hero;
  }
  set setaAge(value) {
    if (typeof value === "string") {
      this.age = value;
    } else {
      this.age = null;
    }
  }
  set setHero(value) {
    if (typeof value === "string") {
      this.name = value;
    } else {
      this.hero = null;
    }
  }
  get getAge() {
    if (this.age === null) {
      return `value belum di set`;
    }
    return this.age;
  }
  set getHero() {
    if (this.hero === null) {
      return `value belum di set`;
    }
    return this.hero;
  }
  getDetail() {
    return `I am ${this.hero}. I am ${this.age} years old and I come from ${this.city}. I can help ${this.name} with ${this.ability}`;
  }
}
